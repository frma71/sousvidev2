class MQTT : public PubSubClient {
public:
  MQTT() : PubSubClient(espClient) { instance = this; }
  void begin();
  void run();
private:
  void callback(char* topic, byte* payload, unsigned int length);
  static void _callback(char* topic, byte* payload, unsigned int length) {
    instance->callback(topic, payload, length);
  }
  static MQTT* instance;
  int lastConnect;
  const char* mqtt_server = "broker.mqtt-dashboard.com";
  WiFiClient espClient;
};
  
MQTT* MQTT::instance;
void MQTT::begin() {
  setServer(mqtt_server, 1883);
  setCallback(_callback);
  lastConnect = 0;
}
void MQTT::run() {
  if (!connected() && millis() - lastConnect > 5000) {
    lastConnect = millis();
    if(connect("frma/sousvide")) {
      Info.println("mqtt subscribe");
      subscribe("frma/sousvide/setpoint");
    }
  }
  loop();
}
void MQTT::callback(char* topic, byte* payload, unsigned int length) {
  char buff[128];
  int len = length > (sizeof(buff)-1) ? sizeof(buff)-1 : length;
  Debug.println(String("Got topic: ") + topic);
  Debug.print("Data:");
  memcpy(buff, payload, len);
  buff[len] = 0;
  Debug.println(buff);
  regulator.setSetPoint(String(buff).toFloat());
}
  
