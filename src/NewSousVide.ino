#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <WiFiManager.h>
#include <PID_v1.h>
#include <OneWire.h>
#include <DallasTemperature.h>
//#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <FS.h>

extern "C" {
  #include <user_interface.h>
}

#include "NtpTime.h"
#include "Config.h"
#include "Timer.h"
#include "HTTPFileServer.h"


WiFiManager wifiManager;
Stream& Debug = Serial;
Stream& Error = Serial;
Stream& Info = Serial;

Config conf;

inline int min(int a, int b) { return a>b?b:a;}
inline int max(int a, int b) { return a<b?b:a;}
inline unsigned int umin(unsigned int a, unsigned int b) { return a>b?b:a;}

#define DATALOG_SHORT_SIZE 60    // Every seconds
#define DATALOG_MEDIUM_SIZE (60) // Every minute
#define DATALOG_LONG_SIZE (7*24) // Every hour
class DataLog2 {
  struct DataItem {
    unsigned long  time:32;
    unsigned short set:8;
    unsigned short tempMin:9;
    unsigned short tempMax:9;
    unsigned short out:6;
  };
public:
  void begin() {
    dataShortIdx = 0;
    dataMediumIdx = 0;
    dataLongIdx = 0;
  }
  void sumInto(struct DataItem* items, int len, struct DataItem& dst) {
    int t = 0xffffffffUL;
    int tempMin = 0x1ff;
    int tempMax = 0;
    int avgOut = 0;
    int avgSet = 0;

    for(int i = 0; i < len; i++) {
      struct DataItem* item = &items[i];

      t = umin(t, item->time);
      tempMin = min(tempMin, item->tempMin);
      tempMax = max(tempMax, item->tempMax);
      avgOut += item->out;
      avgSet += item->set;
    }
    dst.time = t;
    dst.set = avgSet/len;
    dst.tempMin = tempMin;
    dst.tempMax = tempMax;
    dst.out = avgOut/len;
  }
  void add(unsigned long time, double set, double temp, int out) {
    data[dataShortIdx].time = time;
    data[dataShortIdx].set  = (unsigned short)(set*2);
    data[dataShortIdx].tempMin = (unsigned short)(temp*4);
    data[dataShortIdx].tempMax = (unsigned short)(temp*4);
    data[dataShortIdx].out = out/2;
    dataShortIdx++;
    if(dataShortIdx == DATALOG_SHORT_SIZE) {
      sumInto(data, dataShortIdx, dataMedium[dataMediumIdx]);
      dataShortIdx = 0;
      dataMediumIdx++;
    }
    if(dataMediumIdx == DATALOG_MEDIUM_SIZE) {
        sumInto(dataMedium, dataMediumIdx, dataLong[dataLongIdx]);
        dataMediumIdx = 0;
        dataLongIdx++;
    }
    if(dataLongIdx == DATALOG_LONG_SIZE) {
        dataLongIdx = 0;
    }
  }
  String mkJsonItem(struct DataItem& item) {
    String s = " {";
    s = s + "\"time\":\"";
    s = s + String(item.time);
    s = s + "\",\"set\":\"";
    s = s + String(item.set/2.0);
    s = s + "\",\"min\":\"";
    s = s + String(item.tempMin/4.0);
    s = s + "\",\"max\":\"";
    s = s + String(item.tempMax/4.0);
    s = s + "\",\"out\":\"";
    s = s + String(item.out*2.0);
    s = s + "\"}";
    return s;
  }
  void sendData(struct DataItem* items, int start, int len, WiFiClient c, int& first) {
    for(int i = 0; i < len; i++) {
      int idx = (start + i);
      if(idx >= len)
        idx -= len;
      struct DataItem& item = items[idx];
      if(item.time != 0) {
        if(!first)
          c.write(",", 1);
        first = 0;
        String s = mkJsonItem(item);
        c.write(s.c_str(), s.length());
      }
    }
  }
  void sendJson(WiFiClient c) {
    int first = 1;
    c.write("[", 1);
    sendData(dataLong,   dataLongIdx,   DATALOG_LONG_SIZE, c, first);
    sendData(dataMedium, dataMediumIdx, DATALOG_MEDIUM_SIZE, c, first);
    sendData(data,       dataShortIdx,  DATALOG_SHORT_SIZE, c, first);
    c.write("]\n", 2);


  }
private:
  struct DataItem dataLong[DATALOG_LONG_SIZE];
  struct DataItem dataMedium[DATALOG_MEDIUM_SIZE];
  struct DataItem data[DATALOG_SHORT_SIZE];
  int dataShortIdx;
  int dataMediumIdx;
  int dataLongIdx;
  unsigned long startTime;
};

#define relayPin  0
#define OneWirePin 4
#define PumpPin 5



DataLog2 dataLog;

class Regulator {
public:
  Regulator() :
  pid(&input, &output, &setpoint, conf.regP, conf.regI, conf.regD, DIRECT),
  oneWire(OneWirePin),
  sensors(&oneWire) {}
  void begin() {
    windowStartTime = millis();
    setpoint = conf.setPoint;
    pid.SetOutputLimits(0, 1000);
    pid.SetMode(AUTOMATIC);
    pid.SetSampleTime(conf.sampleTime);
    updatePID();
    sampleTimer.begin([this](void* arg) {
      this->sample();
    }, conf.sampleTime, conf.sampleTime);
    sensorCheckOutputMax = true;
    sensorCheckTimer.begin([this](void* arg) {
      if(input > 90) {
        Error.println("Temp over 90C, set setpoint to 0");
        setpoint = 0;
      }
      if(input == 0) {
        Error.println("Temp 0C, set setpoint to 0");
        setpoint = 0;
      }
      // FIXME: Disabled since slowcoocker is too slow.
      if(0 && sensorCheckOutputMax) {
        if(sensorCheckTemp + 1 > input) {
          Error.println("Temp not increasing, set setpoint to 0");
          setpoint = 0;
        }
        else {
          Debug.println("Temp was increasing properly");
        }
      }
      else {
        Debug.println("Output wasn't at 100% the last period");
      }
      sensorCheckTemp = input;
      sensorCheckOutputMax = true;
    }, 30000, 30000);

    sensors.begin();
    //sensors.setWaitForConversion(false);
    if(sensors.getAddress(tempAddr, 0))
    Info.println("Found temp device");

    sensors.requestTemperatures();
    pinMode(relayPin, OUTPUT);
    digitalWrite(relayPin, false);
    wasOn = false;

    dataLog.begin();
  }
  void run() {
    sampleTimer.run();
    // DISABLED: Slowcoocker too slow
    sensorCheckTimer.run();
  }
  int getSampleCount() {
    return sampleCount;
  }
  double getP() { return pid.GetKp(); }
  double getI() { return pid.GetKi(); }
  double getD() { return pid.GetKd(); }
  String getState() {
    String s;
    s = "[" + String(pid.GetMode()) + "," + String(pid.GetKp()) + "," + String(pid.GetKi()) + "," + String(pid.GetKd()) + "]";
    return s;
  }
  double getOutput() { return output/10; }
  double getInput() { return input; }
  double getSetPoint() { return setpoint; }
  void setSetPoint(double setPoint) { this->setpoint = setPoint; }
  void updatePID() {
    pid.SetTunings(conf.regP, conf.regI, conf.regD);
    pid.SetSampleTime(conf.sampleTime);
  }
private:
  void sample(void);
private:
  Timer sampleTimer;
  // State for the sensor checker.
  Timer sensorCheckTimer;
  double sensorCheckTemp;
  int sensorCheckOutputMax;
  // Number of times we failed to read temp sensor without succeeding.
  int failCount;

  int sampleCount;
  double setpoint, input, output;
  bool wasOn = false;
  PID pid;
  int relayPeriod = 10000; // 10 seconds
  int maxOutput = 1000; // 100 * 0.1%
  unsigned long windowStartTime;
  OneWire oneWire;
  DallasTemperature sensors;
  DeviceAddress tempAddr;
};

class JsonGen {
public:
  JsonGen(int bufSize)
  {
    buff = new char[bufSize];
    buffSize = bufSize;
  }
  void start() {
    len = 0;
    buff[0] = 0;
    append("{");
    first = 1;
  }
  void end() {
    append("}");
  }
  void write(Stream& s) {
    while(len & 3) // Potentiall workaround for webserver unaligned access problem
    append(" ");
    s.write((const char*)buff, len);
  }
  void push(const char* name) {
    if(!first) {
      append(",");
    }
    first = 1;
    append("\"");
    append(name);
    append("\":{");
  }
  void pop() {
    append("}");
  }
  void elem(const char* name, const char* val) {
    if(!first)
    append(",");
    first = 0;
    append("\"");
    append(name);
    append("\":\"");
    append(val);
    append("\"");
  }
  void elem(const char* name, int val) {
    char buff[16];
    itoa(val, buff, 10);
    elem(name, buff);
  }
  void elem(const char* name, unsigned long val) {
    elem(name, (int)val); // FIXME:
  }
  void elem(const char* name, double val, int decimals=1) {
    String s = String(val, decimals);
    elem(name, s.c_str());
  }
  int length() {
    return strlen(buff);
  }
  const char* c_str() {
    return buff;
  }
private:
  void append(const char* str) {
    int l = strlen(str);
    if(len + l < buffSize) {
      strcpy(&buff[len], (char*)str);
      len += l;
    } else {
      Debug.println("JSON buffer overflow");
    }
  }
  int first;
  int buffSize;
  char* buff;
  int len;
};


class Pump {
public:
  void begin(void) {
    pinMode(PumpPin, OUTPUT);
    digitalWrite(PumpPin, 1);
  }
  void run(void) {
  }
  void set(int speed) {
    digitalWrite(PumpPin, speed > 0?1:0);
  }
};

class WebServer {
public:
  WebServer(Regulator& regarg, Pump& pumparg, NtpTime& ntparg) :
  server(80),
  regulator(regarg),
  pump(pumparg),
  json(512),
  ntp(ntparg),
  httpUpdater(true){}
  void begin() {
    message = "OK";
    httpUpdater.setup(&server);
    fileServer.setup(&server);
#if 0
    if (mdns.begin("sousvide", WiFi.localIP())) {
      Serial.println("MDNS responder started");
    }
#endif
  server.on("/format", [this]() {
    SPIFFS.format();
    server.send(200,"text/plain", "FORMATTED");
  });
  server.on("/restart", [this]() {
    server.send(200,"text/plain", "RESTARTING");
    ESP.restart();
  });
  server.on("/history", [this]() {
    server.setContentLength(CONTENT_LENGTH_UNKNOWN);
    server.send(200,"text/json", "");
    dataLog.sendJson(server.client());
  });
  server.on("/json", [this]() {
    json.start();
    json.elem("samples", this->regulator.getSampleCount());
    json.elem("setpoint", this->regulator.getSetPoint());
    json.elem("input", this->regulator.getInput());
    json.elem("output", this->regulator.getOutput());
    json.elem("epoch", this->ntp.getEpoch());
    json.elem("uptime", millis()/1000);
    json.elem("p", this->regulator.getP(),3);
    json.elem("i", this->regulator.getI(),3);
    json.elem("d", this->regulator.getD(),3);
    json.elem("regulator", this->regulator.getState().c_str());
    json.elem("msg", message.c_str());
    json.end();
    server.send(200,"text/json", json.c_str());
  });
  server.on("/set", [this]() {
    String a;

    if(server.hasArg("setpoint")) {
      conf.setPoint = String(server.arg("setpoint")).toFloat();
      this->regulator.setSetPoint(conf.setPoint);
    } else if(server.hasArg("pump")) {
      pump.set(String(server.arg("pump")).toInt());
      message = "pump set";
    } else if(server.hasArg("p")) {
      conf.regP = String(server.arg("p")).toFloat();
      this->regulator.updatePID();
    } else if(server.hasArg("i")) {
      conf.regI = String(server.arg("i")).toFloat();
      this->regulator.updatePID();
    } else if(server.hasArg("d")) {
      conf.regD = String(server.arg("d")).toFloat();
      this->regulator.updatePID();
    } else if(server.hasArg("P")) {
      conf.relayPeriod = String(server.arg("P")).toFloat();
      this->regulator.updatePID();
    } else {
      Info.println("Update unknown variable");
      server.send(400,"text/plain", "NOK - Unknown value");
      return;
    }
    conf.save();
    server.send(200,"text/plain", "OK");
  });
  server.begin();
}
void run(void) {
  server.handleClient();
}
private:
  String message;
  ESP8266WebServer server;
  Regulator& regulator;
  Pump& pump;
  NtpTime& ntp;
  JsonGen json;
  ESP8266HTTPUpdateServer httpUpdater;
  HTTPFileServer fileServer;
  MDNSResponder mdns;
};


Regulator regulator;
Pump pump;
NtpTime ntp;
WebServer web(regulator, pump, ntp);



void Regulator::sample() {
  float newTemp = sensors.getTempC(tempAddr);
  if(newTemp == -127) {
    if(failCount++ > 10) {
      Error.println("Failed to read temperature, set setpoint to 0");
      newTemp = 0;
      setpoint = 0;
    }
    Info.println("Failed to read temp");
  } else {
    failCount = 0;
    input = newTemp;
  }
  sensors.requestTemperatures();

  if(setpoint - input >= 5 && pid.GetMode() == AUTOMATIC) {
    Debug.println("Go to MANUAL");
    output = 1000;
    pid.SetMode(MANUAL);
  }
  if(setpoint - input < 5) {
    if(pid.GetMode() == MANUAL) {
      Info.println("Go to AUTOMATIC");
      output = 0;
      pid.SetMode(AUTOMATIC);
    }
    pid.Compute();
  }
  Info.print("Time:");
  Info.print(millis());
  Info.print(" SetPoint:");
  Info.print(setpoint);
  Info.print(" Input:");
  Info.print(input);
  Info.print(" Output:");
  Info.print(output);
  Info.print(" Automatic:");
  Info.println(pid.GetMode());

  unsigned long now = millis();

  if(output != 1000)
  sensorCheckOutputMax = false;

  // Start next window
  if(now - windowStartTime > conf.relayPeriod)
  {
    windowStartTime += conf.relayPeriod;
  }
  bool on = output*conf.relayPeriod > 1000*(now - windowStartTime);
  if(on ^ wasOn) {
    digitalWrite(relayPin, on);
    wasOn = on;
    Debug.println("Relay: " + String(on));
  }
  dataLog.add(ntp.getEpoch(), getSetPoint(), getInput(), getOutput());

  sampleCount++;
}


void setup() {
  Serial.begin(115200);
  Info.println("Booting, version 1");

  int defstate = OUTPUT;
  pinMode(0, OUTPUT); // Relay pin
  //pinMode(1, defstate); // Tx
  pinMode(2, defstate); // Onewire
  //pinMode(3, defstate); // Rx
  pinMode(4, defstate); // NC
  pinMode(5, defstate);       // MC
  //pinMode(6, defstate);       // SPICLK
  //pinMode(7, defstate);       // SPIQ
  //pinMode(8, defstate);       // SPID
  //pinMode(9, defstate);       // SPIHD
  //pinMode(10, defstate);      // SPIWP
  //pinMode(11, defstate);      // SPICS0
  pinMode(12, defstate);      // NC
  pinMode(13, defstate);      // NC
  pinMode(14, defstate);      // NC
  pinMode(15, defstate);      // NC
  pinMode(16, INPUT_PULLDOWN_16);      // NC

  SPIFFS.begin();
  conf.load();
  Info.println("Restart cause: " + String(ESP.getResetInfo()));
  if(ESP.getResetInfoPtr()->reason == REASON_DEFAULT_RST) {
    Info.println("A cold restart, set temp to 0");
    conf.setPoint = 0;
    conf.save();
  }

  WiFi.hostname("sousvide");
  wifiManager.autoConnect("sousvide", "gurgel42");
  ntp.begin();
  regulator.begin();
  pump.begin();
  web.begin();
  Info.print("IP address: ");
  Info.println(WiFi.localIP());
}

void loop() {
  ntp.run();
  web.run();
  pump.run();
  regulator.run();
}
