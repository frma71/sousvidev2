#ifndef _NTPTIME_H_
#define _NTPTIME_H_

extern Stream& Debug;

class NtpTime {
private:
	WiFiUDP mUdp;
	unsigned long mOffset;
	unsigned long mRequestSent = 0;
public:
	void begin() { _begin(); }
	void run() { _run(); }
	unsigned long getEpoch() { return _getEpoch(); }
	unsigned long getOffset() { return mOffset; }

private:
	void _begin() {
	    int rv = mUdp.begin(123);
	    if(!rv) 
	      Debug.println("NtpTime: mUdb.begin(123) failed");
	    sendRequest();
	}
	unsigned long _getEpoch() {
		if(mOffset)
			return mOffset + millis()/1000;
		else {
			return 0;
		}
	}
	void _run() {
		// Resend request every 10 minutes if it's not the first request.
		// ... or every 10 seconds if we dont.
		if(millis() - mRequestSent > 10*60*1000 ||
			(mOffset == 0 && millis() - mRequestSent > 10000))
		{
		  sendRequest();
		  mRequestSent = millis();
		}
		int pktLen = mUdp.parsePacket();

		if(pktLen != 48)
		  return;

		// Skip 40 bytes
		for (int i = 0; i < 40; ++i)
		  mUdp.read();

		unsigned long time = 0;
		for (int i = 0; i < 4; i++)
		  time = time << 8 | mUdp.read();

		mUdp.flush();

		mOffset = (time - 2208988800UL) - millis()/1000;   // convert NTP time to Unix time
		Debug.print("Got epoch ");
		Debug.println(time - 2208988800UL);
	}
	void sendRequest() {
	    static const unsigned long ntpReq[48/4] = { 0xEC0600E3UL, 0};
	    int rv;
	    rv = mUdp.beginPacket("pool.ntp.org", 123);
	    if(!rv) {
	      Debug.println("NtpTime: Failed to create udp request");
	    }
	    rv = mUdp.write((byte *)&ntpReq, 48);
	    if(rv != 48) {
	      Debug.println("NtpTime: Failed to send udp request");
	    }
	    rv = mUdp.endPacket();
	    if(!rv) {
	      Debug.println("NtpTime: Failed to finish udp request");
	    }
	}
};

#endif // _NTPTIME_H_
