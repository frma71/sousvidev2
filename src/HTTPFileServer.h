#include <FS.h>

class ESP8266WebServer;

class HTTPFileServer {
public:
  void setup(ESP8266WebServer* webServer);
private:
  String getContentType(bool download, String filename);
  File fsUploadFile;
};
