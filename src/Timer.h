class Timer {
  public:
  void begin(std::function<void(void*)> func, unsigned int first, unsigned int interval, void* arg = 0) {
    this->func = func;
    this->arg = arg;
    this->interval = interval;
    this->next = millis() + first;
  }
  void run(int now = 0) {
    if(now == 0)
      now = millis();
    if(next - now > 0xffff0000UL) {
      func(arg);   
      next = next + interval;
    }
  }
  private:
    std::function<void(void*)> func;
    void* arg;
    unsigned int interval;
    unsigned int next;
};

