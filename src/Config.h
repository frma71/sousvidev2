
class Config {
  const char* fileName = "/config.json";
public:
  void load() {
    bool validConf = false;
    if(SPIFFS.exists(fileName)) {
      Serial.println("config.json exists");
      File configFile = SPIFFS.open(fileName, "r");
      if (configFile) {
        Serial.println("config.json opened");
        size_t size = configFile.size();
        std::unique_ptr<char[]> buf(new char[size]);
        configFile.readBytes(buf.get(), size);
        StaticJsonBuffer<512> jsonBuffer;
        
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        
        if (json.success()) {
          if(json["version"] == VERSION) {
            Serial.println("Right configuration version");
            validConf = true;
            regP = json["regP"];
            regI = json["regI"];
            regD = json["regD"];
            relayPeriod = json["relayPeriod"];
            setPoint = json["setPoint"];
            sampleTime = json["sampleTime"];
            validConf = true;
          }
          else {
            Serial.println("Wrong configuration version");
          }
        } else {
          Serial.println("Failed to parse json");
        }
      }
    }
    else {
      Serial.println("config.json doesn't exist");
    }
    if(!validConf) {
      Serial.println("Loading defaults");
      regP = 125;
      regI = 0.37;
      regD = 0;
      relayPeriod = 10000;
      setPoint = 0;
      sampleTime = 1000;
      save();
    }
  }
  int save() {
      DynamicJsonBuffer jsonBuffer;
      JsonObject& json = jsonBuffer.createObject();
      json["version"] = VERSION;
      json["regP"] = regP;
      json["regI"] = regI;
      json["regD"] = regD;
      json["relayPeriod"] = relayPeriod;
      json["setPoint"] = setPoint;
      json["sampleTime"] = sampleTime;
      File configFile = SPIFFS.open(fileName, "w");
      if(!configFile) {
        Serial.println("failed to open config file for writing");
      }
      else {
        json.printTo(Serial);
        json.printTo(configFile);
        configFile.close();
      }
  }
public:
  static int VERSION;
  uint8_t version;
  float regP;
  float regI;
  float regD;
  int relayPeriod; // ms
  float setPoint;
  int sampleTime; // ms
};

int Config::VERSION = 4;

