class Tunnel {
public:
  void begin(int localPort, String remoteHost, int remotePort) {
    this->localPort = localPort;
    this->remoteHost = remoteHost;
    this->remotePort = remotePort;
  }
  void run(void) {
    if(!local.connected()) {
      if(remote.connected())
	remote.stop();
      local.connect("127.0.0.1", localPort);
    }
    if(local.connected() && !remote.connected()) 
      remote.connect(remoteHost.c_str(), remotePort);
    if(local.connected() && remote.connected()) {
      size_t rv;
      if(local.available()) {        
	rv = local.read((uint8_t*)buff, (size_t)sizeof(buff));
	if(rv > 0)
	  remote.write((uint8_t*)buff, rv);
      }
      if(remote.available()) {
	rv = remote.read((uint8_t*)buff, (size_t)sizeof(buff));
	if(rv > 0)
	  local.write((uint8_t*)buff, rv);
      }
    }
  }
private:
  uint8_t buff[256];
  int localPort;
  String remoteHost;
  int remotePort;
  WiFiClient remote;
  WiFiClient local;
};
